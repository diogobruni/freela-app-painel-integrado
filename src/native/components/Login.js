import React from 'react'
import PropTypes from 'prop-types'
import ReactNative, { Image, StyleSheet, Alert } from 'react-native'

/* Import Modules */
import { Facebook, Google } from 'expo'

/* Import Constants */
import { Facebook as FacebookData, Google as GoogleData } from '../../constants/social'

/* Import Actions */
import { Actions } from 'react-native-router-flux'

/* Import UI Components */
import { H1, Container, Content, Form, Item, Label, Input, Text, Button, View, Icon, Toast } from 'native-base'
import Loading from './Loading'
import LoadingOverlay from './LoadingOverlay'
import Messages from './Messages'
import Header from './Header'

/* Import Styles */
import { cssColors, cssDevice, cssSocialButtons, cssUtils } from '../styles'

/* Data Controls */
import { translate } from '../../i18n'


const imgLogo = require('../../images/logo-white.png')

class Login extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    user: PropTypes.object.isRequired,
    onSocialLogin: PropTypes.func.isRequired,
  }

  static defaultProps = {
    // error: null,
    // locale: null,
    user: {},
  }

  constructor(props) {
    super(props);

    this.state = {
      sceneName: 'login',
      ready: false,
      email: 'diogobruni@gmail.com',
      password: '!Diogob14!'
    }

    this.handleFacebookLogin = this.handleFacebookLogin.bind(this)
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleLoginForm = this.handleLoginForm.bind(this)
  }

  componentWillMount() {
    //console.log(this.props.member)
  }

  componentDidMount() {
    this.setState({ ready: true })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const defaultToastParams = {
      position: 'top',
      buttonText: 'Okay',
      duration: 5000
    }

    if (prevProps.error != this.props.error) {
      if (this.props.error) {
        Toast.show({
          ...defaultToastParams,
          text: this.props.error,
          type: 'danger',
        })
      } else {
        Toast.toastInstance._root.closeToast()
      }
    }

    if (prevProps.success != this.props.success) {
      if (this.props.success) {
        Toast.show({
          ...defaultToastParams,
          text: this.props.success,
          type: 'success',
        })
      } else {
        Toast.toastInstance._root.closeToast()
      }
    }

    if (prevProps.info != this.props.info) {
      if (this.props.info) {
        Toast.show({
          ...defaultToastParams,
          text: this.props.info,
          type: 'info',
        })
      } else {
        Toast.toastInstance._root.closeToast()
      }
    }

    if (this.props.user.profile && Actions.currentScene == this.state.sceneName) {
      Actions.dashboard()
    }
  }

  handleChangeEmail(value) {
    this.setState({ email: value })
  }

  handleChangePassword(value) {
    this.setState({ password: value })
  }

  async handleLoginForm(event) {
    const { email, password } = this.state
    
    const data = {
      username: email,
      password: password
    }

    this.props.passwordLogin(data)
      .then((status) => {
        if (status) {
          this.props.getSessionUser()
        }
      })
  }

  async handleFacebookLogin(event) {
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(FacebookData.app.id, {
      permissions: FacebookData.app.scopes,
      behavior: 'browser'
    })

    if (type === 'success') {
      const data = {
        connection: 'facebook',
        accessToken: token,
        refreshToken: '',
        // email: email
      }
      this.props.onSocialLogin(data)
        .then((status) => {
          if (status) {
            this.props.getSessionUser()
          }
        })
    } else {
      Alert.alert(
        'Erro',
        'Não foi possível efetuar a autenticação'
      )
    }
  }

  async handleGoogleLogin(event) {
    const result = await Google.logInAsync(GoogleData.app)

    if (result.type === 'success') {
      const data = {
        connection: 'google',
        accessToken: result.accessToken,
        refreshToken: result.refreshToken,
        // email: result.user.email
      }
      this.props.onSocialLogin(data)
        .then((status) => {
          if (status) {
            this.props.getSessionUser()
          }
        })
    } else {
      Alert.alert(
        'Erro',
        'Não foi possível efetuar a autenticação'
      )
    }
  }

  render() {
    const { loading, error, locale } = this.props
    const { ready, email, password } = this.state

    if (loading && !ready) {
      return <Loading />
    }

    return (
      <Container style={localStyles.loginBackground}>
        {loading && <LoadingOverlay />}
        <Content>
          <View style={[cssUtils.padded, cssUtils.block, cssUtils.center]}>
            <View style={[cssUtils.block, cssUtils.center]}>
              <Image source={imgLogo} style={[localStyles.logoImage, { marginBottom: 30 }]} />
              <H1 style={[localStyles.textWhite, { marginBottom: 20 }]}>Opa, tudo bom?</H1>
              <Text style={[localStyles.textWhite, cssUtils.textNormalSize]}>Entre ou crie uma conta com seu email.</Text>
            </View>

            <Form style={[localStyles.loginForm, cssUtils.center]}>
              <Item floatingLabel style={{ marginLeft: 15, marginRight: 15 }}>
                <Label style={localStyles.textWhite}>E-mail</Label>
                <Input style={localStyles.textWhite} name="email" onChangeText={this.handleChangeEmail} value={email} />
              </Item>

              <Item floatingLabel style={{ marginLeft: 15, marginRight: 15 }}>
                <Label style={localStyles.textWhite}>Senha</Label>
                <Input style={localStyles.textWhite} secureTextEntry={true} name="password" onChangeText={this.handleChangePassword} value={password} />
              </Item>

              <Button block rounded light style={[{ marginTop: 15 }]} onPress={this.handleLoginForm}>
                <Text>Entrar</Text>
              </Button>
            </Form>

            <View style={[localStyles.socialLoginBox, cssUtils.block, cssUtils.center, { marginTop: 50 }]}>
              <View style={[cssUtils.divider, cssUtils.dividerWhite, { marginBottom: 40 }]}></View>

              <Text style={[localStyles.textWhite, cssUtils.textXNormalSize, { marginBottom: 20 }]}>Ou acesso por alguma das redes abaixo:</Text>

              <Button onPress={this.handleFacebookLogin} style={[cssSocialButtons.loginBtn, cssSocialButtons.facebook, cssUtils.block, { marginBottom: 15 }]}>
                <Icon name="logo-facebook" style={cssSocialButtons.btnIcon} />
                <Text style={cssSocialButtons.btnText}>Acessar via Facebook</Text>
              </Button>

              <Button onPress={this.handleGoogleLogin} style={[cssSocialButtons.loginBtn, cssSocialButtons.google, cssUtils.block]}>
                <Icon name="logo-google" style={cssSocialButtons.btnIcon} />
                <Text style={cssSocialButtons.btnText}>Acessar via Google</Text>
              </Button>
              
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}



const localStyles = StyleSheet.create({
  textWhite: {
    color: '#FFF'
  },
  loginBackground: {
    backgroundColor: cssColors.main
  },
  logoImage: {
    //marginTop: device.height / 8,
    marginTop: 40,
    width: cssDevice.width / 3,
    height: cssDevice.width / 3,
  },
  socialLoginBox: {
  }
})

export default Login;
