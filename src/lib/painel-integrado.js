import apiData from '../constants/painel-integrado'
import moment from 'moment'
import accounting from 'accounting'
//import axios from 'axios'
import { objSerialize } from '../lib/helper.js'

class PainelIntegrado {
	constructor() {
		this.defaultXHRRequest = {
			headers: {
	      'Content-Type': 'application/x-www-form-urlencoded',
	      //'Content-Type': 'application/json',
      	'Accept': 'application/json',
      	'Cache-Control': 'no-cache, no-store, must-revalidate',
		    'Pragma': 'no-cache',
		    'Expires': 0,
	    },
		}
	}

	async Request(uri, extraRequestOptions = {}, extraActions = { return: true }) {
		try {
			const requestOptions = {
				...this.defaultXHRRequest,
				...extraRequestOptions
				// body: JSON.stringify(params)
			}
			const response = await fetch(uri, requestOptions)

			if (extraActions.return) {
				const json = await response.json()
				return {
					response,
					json: json
				}
			}
		} catch(err) {
			throw err
			return err
		}
	}

	async Post(uri, data, extraOptions = {}) {
		try {
			const requestOptions = {
				...this.defaultXHRRequest,
				...extraOptions
			}

			const response = await axios.post(uri, data, requestOptions)
			return {
				response,
				json: await response.json()
			}
		} catch(err) {
			throw err
			return false
		}
	}

	async AuthPassword(data) {
		try {
			const uri = apiData.endpoint.login
			const dataObj = {
				u: data.username,
				p: data.password
			}

			const requestOptions = {
				method: 'POST',
				//body: `u=${data.username}&p=${data.password}`
				body: objSerialize(dataObj)
			}

			const objResponse = await this.Request(uri, requestOptions)
			return objResponse

			/*
			const uri = apiData.endpoint.login
			const body = {
				u: data.username,
				p: data.password
			}
			const objResponse = await this.Post(uri, body)
			return objResponse
			*/
		} catch(err) {
			throw err
			return false
		}
	}

	async AuthSocial(data) {
		try {
			const { accessToken, connection } = data
			const uri = apiData.endpoint.loginSocial(connection, accessToken)
			
			const objResponse = await this.Request(uri, { method: 'GET' })
			return objResponse
		} catch(err) {
			throw err
			return false
		}
	}

	async getSessionUser() {
		try {
			const uri = apiData.endpoint.getUserProfile

			const objResponse = await this.Request(uri, { method: 'GET' })
			return objResponse
		} catch(err) {
			throw err
			return false
		}
	}

	async logout() {
		try {
			const uri = apiData.endpoint.logout

			/*
			const objResponse = await this.Request(uri, { method: 'GET' })
			return objResponse
			*/

			this.Request(uri, { method: 'GET' }, { return: false })
			return true
		} catch(err) {
			throw err
			return false
		}
	}

	//async getPanelData(panelId, startDate, untilDate) {
	async getPanelData(params) {
		try {
			const { panelId, dateStart, dateEnd, fbAccount, googleAccount, monetizzeAccount } = params

			const uriParams = {}

			if (panelId != 'dashboard') {
				uriParams.panel_id = panelId
			}

			if (dateStart && dateEnd) {
				dateStart = moment(dateStart, 'DD/MM/YYYY').format('YYYY-MM-DD')
				dateEnd = moment(dateEnd, 'DD/MM/YYYY').format('YYYY-MM-DD')

				uriParams.date_start = dateStart
				uriParams.date_end = dateEnd
			}
			
			let uri = ''
			if (fbAccount) {
				uri = apiData.endpoint.getPanelFacebookAds(uriParams)
			} else if (googleAccount) {
				uri = apiData.endpoint.getPanelGoogleAdsense(uriParams)
			} else if (monetizzeAccount) {
				uri = apiData.endpoint.getPanelMonetizze(uriParams)
			} else {
				uri = apiData.endpoint.getPanel(uriParams)
			}

			const objResponse = await this.Request(uri, { method: 'GET' })
			return objResponse
		} catch(err) {
			throw err
			return err
		}
	}

	formatPanelData(data) {
		const fieldsFormat = {
			CURRENCY: [ 'revenue', 'spend', 'earn', 'facebook_ads_spend', 'google_adsense_earn', 'monetizze_earn' ],
			PERCENT: [ 'roi' ],
		}

		for(let key in data) {
			let newObj = {
				value: data[key],
				formattedValue: ''
			}

			if (fieldsFormat.CURRENCY.includes(key)) {
				newObj.formattedValue = accounting.formatMoney(newObj.value, "R$ ", 2, ".", ",")
			} else if (fieldsFormat.PERCENT.includes(key)) {
				newObj.formattedValue = parseFloat(newObj.value * 100).toFixed(1).toString().replace('.', ',') + "%"
			} else {
				newObj.formattedValue = newObj.value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
			}

			data[key] = newObj
		}

		return data
	}

	dealPanelJSON(data, startDate, untilDate, panelId = 'dashboard', account = 'all') {
		try {
			startDate = moment(startDate, 'DD/MM/YYYY')
			untilDate = moment(untilDate, 'DD/MM/YYYY')
			const dealData = {
				//json: data,
				spend: {
			  	value: 0,
		  		formattedValue: 'R$ 0,00',
			  	subCategories: []
			  },
			  earn: {
			  	value: 0,
		  		formattedValue: 'R$ 0,00',
			  	subCategories: []
			  },
			  revenue: {
			  	value: 0,
		  		formattedValue: 'R$ 0,00',
			  },
			  roi: {
			  	value: 0,
		  		formattedValue: 'R$ 0,00',
			  },
			  facebook_ads: {},
			  updatedAt: data.updatedAt
			}

			const { sumStats, aveStats, moneyValues, percentValues } = this.getDealDataPreVars(panelId)
	    const statsObj = {}

			for(let iDay = startDate.clone(); iDay.isSameOrBefore(untilDate); iDay.add(1, 'day')) {
				let arData = undefined

				if (data && data.dashboard) {
					if (account == 'all') {
						if (
							data.dashboard.root_level &&
							data.dashboard.root_level[iDay.format('YYYY-MM-DD')]
						) {
							arData = data.dashboard.root_level[iDay.format('YYYY-MM-DD')]
						}
					} else {
						if (
							data.dashboard.account_level &&
							data.dashboard.account_level[iDay.format('YYYY-MM-DD')] &&
							data.dashboard.account_level[iDay.format('YYYY-MM-DD')][account]
						) {
							arData = data.dashboard.account_level[iDay.format('YYYY-MM-DD')][account]
						}
					}
				}

				if (arData !== undefined) {
					Object.entries(arData).forEach(function([stat, value]) {
						try {
							if (value != '-') {
								if (sumStats.includes(stat)) {
									if (statsObj[stat] === undefined) {
										statsObj[stat] = parseFloat(value)
									} else {
										statsObj[stat] = statsObj[stat] + parseFloat(value)
									}
								} else if (aveStats.includes(stat)) {
									if (statsObj[stat] === undefined) {
										statsObj[stat] = parseFloat(value)
									} else {
										statsObj[stat] = parseFloat(statsObj[stat] + parseFloat(value)) / 2
									}
								}
							}
						} catch(e) {
							throw e
							return false
						}
					})
				}
			}

			//const { categories, subCategories } = this.getDealDataCategoriesAndSubs(panelId)
			const categories = []
			const subCategories = []
			Object.entries(statsObj).forEach(function([stat, value]) {
				const category = matchStringArrayRegex(categories, stat)
				const subCategory = matchStringArrayRegex(subCategories, stat)
				
				let viewValue = ''
				if (moneyValues.includes(stat)) {
					viewValue = accounting.formatMoney(value, "R$ ", 2, ".", ",")
				} else if (percentValues.includes(stat)) {
					//viewValue = parseFloat(value * 100).toFixed(1) + "%"
					viewValue = parseFloat(value * 100).toFixed(1).toString().replace('.', ',') + "%"
				} else {
					//viewValue = value.toLocaleString('pt-BR')
					viewValue = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
				}

				if (category) {
					const [ matchCategory ] = category

					if (subCategory) {
						const [ matchSubCategory ] = subCategory

						dealData[matchCategory].subCategories.push({
							name: matchSubCategory,
							value: value,
							formattedValue: viewValue
						})
					} else {
						dealData[matchCategory].value = value
						dealData[matchCategory].formattedValue = viewValue
					}
				} else {
					dealData[stat] = {
						value: value,
						formattedValue: viewValue
					}
				}
			})

			// CALCULATE ROI AND REVENUE
			if ('earn' in statsObj && 'spend' in statsObj) {
				const revenue = statsObj['earn'] - statsObj['spend']
				const formattedRevenue = accounting.formatMoney(revenue, "R$ ", 2, ".", ",")
				
				let roi = 0
				let formattedRoi = 0
				if (statsObj.spend > 0) {
					roi = revenue / statsObj.spend * 100
					formattedRoi = parseFloat(roi).toFixed(2) + "%"
				}

				dealData.revenue = {
					value: revenue,
					formattedValue: formattedRevenue
				}

				dealData.roi = {
					value: roi,
					formattedValue: formattedRoi
				}
			}

			return { [panelId]: dealData }
		} catch (e) {
			throw e
			return false
		}
	}

	getDealDataPreVars(type = 'default') {
		let sumStats = []
	  let aveStats = []
	  let moneyValues = []
	  let percentValues = []

	  switch (type) {
	  	case 'facebook_ads':
	  		sumStats = ['spend', 'impressions', 'reach', 'clicks', 'conversion_initiate_checkouts', 'conversion_purchases']
		    aveStats = ['cpm', 'clicks_ctr', 'clicks_cpc', 'cpc_initiate_checkout', 'cpc_purchase', 'conversion_rate']
		    moneyValues = ['spend', 'cpm', 'clicks_cpc', 'cpc_purchase']
		    percentValues = ['clicks_ctr', 'conversion_rate']
	  	break
	  	case 'google_adsense':
	  		sumStats = ['earn', 'page_views', 'clicks']
		    aveStats = ['page_views_rpm', 'cost_per_click', 'page_views_ctr']
		    moneyValues = ['earn', 'page_views_rpm', 'cost_per_click']
		    percentValues = ['page_views_ctr']
	  	break
	  	case 'monetizze':
	  		sumStats = ["cancels_value", "cancels", "cc_cancels", "billets_cancels", "gratis_cancels", "amount_canceled", "sales_value", "sales", "amount_sold", "billets_sales", "billets_sales_value", "cc_sales", "cc_sales_value", "gratis_sales", "gratis_sales_value", "pending", "pending_value", "amount_pending", "billets_pending", "billets_pending_value", "cc_pending", "cc_pending_value", "gratis_pending", "gratis_pending_value", "refunds", "refunds_value", "amount_refunded", "disputes", "disputes_value", "amount_disputing", "completes", "completes_value"]
		    aveStats = []
		    moneyValues = ["cancels_value", "sales_value", "billets_sales_value", "cc_sales_value", "gratis_sales_value", "pending_value", "billets_pending_value", "cc_pending_value", "gratis_pending_value", "refunds_value", "disputes_value", "completes_value"]
		    percentValues = []
	  	break
	  	default:
				sumStats = ['earn', 'spend', 'google_adsense_earn', 'monetizze_earn', 'facebook_ads_spend']
			  aveStats = [] // Calculating ROI on the fly
			  moneyValues = ['earn', 'spend', 'google_adsense_earn', 'monetizze_earn', 'facebook_ads_spend']
			  percentValues = []
	  	break
	  }

	  return { sumStats, aveStats, moneyValues, percentValues }
	}

	getDealDataCategoriesAndSubs(type = 'default') {
		let categories = []
		let subCategories = []

		switch (type) {
	  	case 'monetizze':
	  		categories = ['sales', 'refunds', 'pending']
				subCategories = ['facebook_ads', 'google_adsense', 'monetizze']
	  	break
	  	default:
	  		categories = ['earn', 'spend']
				subCategories = ['facebook_ads', 'google_adsense', 'monetizze']
	  	break
	  }

	  return { categories, subCategories }
	}

	async getPanelList() {
		try {
			const uri = apiData.endpoint.getPanelList

			const objResponse = await this.Request(uri, { method: 'GET' })
			return objResponse
		} catch(err) {
			throw err
			return false
		}
	}
}

function matchStringArrayRegex(arRegex, string) {
	for(let i in arRegex) {
		const match = string.match(arRegex[i])
		if (match) {
			return match
		}
	}
	return false
}

export default new PainelIntegrado()