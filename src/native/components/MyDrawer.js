import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Drawer } from 'native-base';
import Sidebar from './Sidebar'
import Navbar from './Navbar'

class MyDrawer extends React.Component {
  static propTypes = {
    drawer: PropTypes.object,
    closeDrawer: PropTypes.func,
    title: PropTypes.string,
  }

  static defaultProps = {
    title: 'Painel Integrado',
  }

  constructor(props) {
    super(props);
  }


  render() {
    const { title } = this.props

    return (
      <Drawer
        ref={(ref) => { this.drawer = ref }}
        content={<Sidebar navigator={this.navigator} />}
      >
        <Navbar
          title={title}
          openDrawer={this.openDrawer.bind(this)}
        />

        {this.props.children}
      </Drawer>
    )
  }
}

const mapStateToProps = state => ({
  drawer: state.drawer || {},
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(MyDrawer)