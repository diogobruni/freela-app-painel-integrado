import React from 'react'
import { Scene, Stack } from 'react-native-router-flux'
import { Icon } from 'native-base'

import DefaultProps from '../constants/navigation'
import AppConfig from '../../constants/config'

import LoginContainer from '../../containers/Login'
import LoginComponent from '../components/Login'

const Login = (
  <Stack>
    <Scene
      hideNavBar
      key="login"
      {...DefaultProps.navbarProps}
      component={LoginContainer}
      Layout={LoginComponent}
    />
  </Stack>
)

export default Login
