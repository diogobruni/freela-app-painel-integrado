import ErrorMessages from '../constants/errors'
import statusMessage from './status'
import { Actions } from 'react-native-router-flux'
import PainelIntegrado from '../lib/painel-integrado'

import { Toast } from 'native-base';

export function getUserData(dispatch) {
  const fakeData = {
    token: 'abc123',
    email: 'diogobruni@gmail.com'
  }

  return dispatch({
    type: 'USER_DATA',
    data: fakeData,
  })
}

export function passwordLogin(data) {
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			await statusMessage(dispatch, 'loading', true)
			const { response, json } = await PainelIntegrado.AuthPassword(data)
			if (response.ok) {
				//const data = await response.json()
				/* Set Fake Token */
				return resolve(dispatch({
					type: 'PASSWORD_LOGIN',
					data: { token: 'fake-token' }
				}))
			} else {
				await statusMessage(dispatch, 'loading', false)
				await statusMessage(dispatch, 'error', json.message || ErrorMessages.auth)
				dispatch({ type: 'USER_RESET' })
				return resolve(false)
			}

		} catch(err) {
			console.log(err)
			await statusMessage(dispatch, 'error', err.message || ErrorMessages.auth)
		}
	}).catch(async (err) => { await statusMessage(dispatch, 'error', err.message || ErrorMessages.auth); throw err.message; });
}

export function socialLogin(data) {
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			await statusMessage(dispatch, 'loading', true)
			const { response, json } = await PainelIntegrado.AuthSocial(data)

			if (response.ok) {
				//const data = await response.json()
				/* Set Fake Token */
				return resolve(dispatch({
					type: 'SOCIAL_LOGIN',
					data: { token: 'fake-token' }
				}))
			} else {
				await statusMessage(dispatch, 'loading', false)
				await statusMessage(dispatch, 'error', ErrorMessages.auth)
				dispatch({ type: 'USER_RESET' })
				return resolve(false)
			}

		} catch(err) {
			await statusMessage(dispatch, 'error', err.message || ErrorMessages.auth)
		}
	}).catch(async (err) => { await statusMessage(dispatch, 'error', err.message || ErrorMessages.auth); throw err.message; });
}

export function getSessionUser() {
	//return 
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			await statusMessage(dispatch, 'loading', true)
			const { response, json } = await PainelIntegrado.getSessionUser()
			await statusMessage(dispatch, 'loading', false)

			if (response.ok) {
				return resolve(dispatch({
					type: 'GET_SESSION_USER',
					data: json
				}))
			} else {
				return resolve(dispatch({ type: 'USER_RESET' }))
			}
		} catch(err) {}
	})
}

export function logout() {
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			await statusMessage(dispatch, 'loading', true)
			//const { response, json } = await PainelIntegrado.logout()
			await PainelIntegrado.logout()
			await statusMessage(dispatch, 'loading', false)

			resolve(dispatch({
				type: 'USER_RESET'
			}))
			Actions.login()
		} catch(err) {}
	})
}