import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { Container, Content, Text, H1, H2, H3, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Header from './Header';

const Profile = ({member, logout}) => (
  <Container>
    <Content>
      <Header
        title="Profile"
        content="Your profile data"
      />
    </Content>
  </Container>
);

export default Profile;
