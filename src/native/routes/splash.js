import React from 'react'
import { Scene, Stack } from 'react-native-router-flux'
import { Icon } from 'native-base'

import DefaultProps from '../constants/navigation'
import AppConfig from '../../constants/config'

import SplashContainer from '../../containers/Splash'
import SplashComponent from '../components/Splash'

const SplashScreen = (
  <Stack>
    <Scene
      hideNavBar
      key="splash"
      {...DefaultProps.navbarProps}
      component={SplashContainer}
      Layout={SplashComponent}
    />
  </Stack>
)

export default SplashScreen
