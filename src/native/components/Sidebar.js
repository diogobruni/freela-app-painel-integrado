import React from 'react';
import PropTypes from 'prop-types';
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux';
import { StyleSheet, Image } from 'react-native'
import { Container, Content, View, List, ListItem, Left, Right, Icon, Text, Badge } from 'native-base'
//import { Header, Body, Button, Title } from 'native-base'
import { cssColors, cssDevice, cssUtils } from '../styles'
import { logout } from '../../actions/user'
import { getCustomPanelList } from '../../actions/filters'
import { closeDrawer } from '../../actions/drawer'
import { setPanelId } from '../../actions/filters'

const imgLogo = require('../../images/logo-white.png')

const localStyles = StyleSheet.create({
  text: {
    fontWeight: cssDevice.platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20
  },
  textSub: {
    fontSize: 13,
    marginLeft: 42
  },
  drawerCover: {
    height: cssDevice.height / 4,
    width: null,
    position: 'relative',
    marginBottom: 10,
    backgroundColor: cssColors.main,
  },
  drawerImage: {
    width: cssDevice.width / 4,
    height: cssDevice.width / 4,
  },
  badgeText: {
    fontSize: cssDevice.platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: cssDevice.platform.OS === "android" ? -3 : undefined
  },
  labelListItem: {
    marginBottom: 0,
    paddingBottom: 0
  },
  labelText: {
    fontSize: 11,
    opacity: 0.7,
  },
  listItemActive: {
    backgroundColor: '#F5F5F5'
  },
  listItemIcon: {
    color: '#777',
    fontSize: 26,
    width: 30
  },
  listItemImgIcon: {
    width: 20,
    height: 20,
    opacity: 0.6
  }
})

class Sidebar extends React.Component {
  static propTypes = {
  }

  static defaultProps = {
  }

  constructor(props) {
    super(props)

    this.state = {
      menuList: this.getMenuList()
    }
  }

  getMenuList() {
    const { filters } = this.props
    const arCustomPanels = []

    if (filters && filters.customPanelList) {
      for(let panel of filters.customPanelList) {
        arCustomPanels.push({
          name: panel.name,
          route: 'dashboard',
          sub: true,
          param: { actionParam: panel.id },
          preAction: {
            function: this.props.setPanelId,
            param: panel.id
          }
        })
      }
    }

    return [
      {
        name: 'Painéis',
        route: false,
        groupLabel: true,
      },
      {
        name: 'Principal',
        route: 'dashboard',
        //icon: 'analytics',
        imgIcon: require('../../images/50/logo-50.png'),
        param: { actionParam: 'dashboard' },
        preAction: {
          function: this.props.setPanelId,
          param: 'dashboard'
        }
      },
      ...arCustomPanels,
      /*{
        name: 'Facebook Ads',
        route: 'panelFacebookAds',
        //icon: 'logo-facebook',
        imgIcon: require('../../images/50/platform-facebook-ads-50.png')
      },
      {
        name: 'Google Adsense',
        route: 'panelGoogleAdsense',
        //icon: 'logo-google',
        imgIcon: require('../../images/50/platform-adsense-50.png')
      },
      {
        name: 'Monetizze',
        route: 'panelMonetizze',
        //icon: 'cash',
        imgIcon: require('../../images/50/platform-monetizze-50.png')
      },*/
      /*{
        name: 'Usuário',
        route: false,
        groupLabel: true,
      },
      {
        name: 'Perfil',
        route: 'profile',
        icon: 'contact',
      },*/
      {
        name: 'Sair',
        route: 'logout',
        icon: 'key',
      },
    ]
  }

  componentDidMount() {
    const that = this
    this.props.getCustomPanelList().then(function() {
      that.setState({
        menuList: that.getMenuList()
      })
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.filters.panelId != this.props.filters.panelId) {
      this.setState({
        menuList: this.getMenuList()
      })
    }
  }

  ListMenuRedirect(item) {
    const { route, param } = item

    if (route) {
      switch(route) {
        case 'logout':
          this.props.logout()
        break
        default:
          if (Actions.currentScene != route) {
            if (Actions[route]) {
              this.MenuClickAction(item, 'push')
              //Actions[route](param)
            }
          } else {
            this.MenuClickAction(item, 'refresh')
            //Actions.refresh(param)
          }
          this.props.closeDrawer()
        break
      }
    }
  }

  MenuClickAction(item, type) {
    const ActionType = (item, type) => {
      const { route, param } = item
      switch(type) {
        case 'refresh':
          Actions.refresh(param)
        break
        default:
          Actions[route](param)
        break
      }
    }

    if (item.preAction) {
      item.preAction.function(item.preAction.param)
        .then(() => {
          ActionType(item, type)
        })
    } else {
      ActionType(item, type)
    }
  }

  render() {
    const { filters } = this.props
    const { menuList } = this.state
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: '#FFF', top: -1 }}
        >
          <View style={[localStyles.drawerCover, cssUtils.center]}>
            <Image
              source={imgLogo}
              style={localStyles.drawerImage}
            />
          </View>

          <List
            dataArray={menuList}
            renderRow={menuItem => {
              let bItemParam = menuItem.param && menuItem.param.actionParam == filters.panelId ? true : false
              let itemActive = Actions.currentScene == menuItem.route ? true : false
              const viewParentStyles = []
              if (itemActive &&  bItemParam) {
                viewParentStyles.push(localStyles.listItemActive)
              }

              const textStyles = [ localStyles.text ]
              if (menuItem.sub) {
                textStyles.push(localStyles.textSub)
              }

              return (<View style={viewParentStyles}>
                {menuItem.groupLabel ? (
                  <ListItem noBorder style={localStyles.labelListItem}>
                    <Left>
                      <Text style={localStyles.labelText}>{menuItem.name.toUpperCase()}</Text>
                    </Left>
                  </ListItem>
                ) : (
                  <ListItem button noBorder onPress={() => { this.ListMenuRedirect(menuItem) }}>
                    <Left>
                      {menuItem.icon &&
                        <Icon active name={menuItem.icon} style={localStyles.listItemIcon} />
                      }
                      {menuItem.imgIcon &&
                        <Image source={menuItem.imgIcon} style={localStyles.listItemImgIcon} />
                      }
                      <Text style={textStyles}>{menuItem.name}</Text>
                    </Left>
                  </ListItem>
                )}
              </View>)
            }} />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  filters: state.filters || {},
})

const mapDispatchToProps = {
  closeDrawer,
  logout,
  getCustomPanelList,
  setPanelId
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)