import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//import { login, getMemberData } from '../actions/auth';
import { passwordLogin, socialLogin, getSessionUser } from '../actions/user'

class Login extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    passwordLogin: PropTypes.func.isRequired,
    getSessionUser: PropTypes.func.isRequired,
    onSocialLogin: PropTypes.func.isRequired,
  }


  render = () => {
    const {
      Layout,
      locale,
      user,
      loading,
      info,
      error,
      success,
      passwordLogin,
      getSessionUser,
      onSocialLogin,
    } = this.props;

    return <Layout
      locale={locale}
      user={user}
      loading={loading}
      info={info}
      error={error}
      success={success}
      passwordLogin={passwordLogin}
      getSessionUser={getSessionUser}
      onSocialLogin={onSocialLogin}
    />;
  }
}

const mapStateToProps = state => ({
  locale: state.locale || null,
  user: state.user || {},
  loading: state.status.loading || false,
  info: state.status.info || null,
  error: state.status.error || null,
  success: state.status.success || null,
})

const mapDispatchToProps = {
  passwordLogin,
  getSessionUser,
  onSocialLogin: socialLogin,
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)