export function closeDrawer() {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'CLOSE_DRAWER',
      data: { generatedTimestamp: + new Date() }
    }))
  })
}

export function openDrawer() {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'OPEN_DRAWER',
      data: { generatedTimestamp: + new Date() }
    }))
  })
}