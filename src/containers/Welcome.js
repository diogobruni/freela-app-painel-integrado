import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Welcome extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
  }

  //componentDidMount = () => this.props.getMemberData();

  render = () => {
    const {
      Layout,
      locale,
      user,
      loading,
      info,
      error,
      success,
    } = this.props;

    return <Layout
      locale={locale}
      user={user}
      loading={loading}
      info={info}
      error={error}
      success={success}
    />;
  }
}

const mapStateToProps = state => ({
  locale: state.locale || null,
  user: state.user || {},
  loading: state.status.loading || false,
  info: state.status.info || null,
  error: state.status.error || null,
  success: state.status.success || null,
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Welcome)