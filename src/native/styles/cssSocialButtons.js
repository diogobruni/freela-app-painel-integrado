import { StyleSheet } from 'react-native'
import { cssDevice } from './'

const buttonHeight = 44
const iconSize = 32

const iconHorizontalMargin = 18
const iconWidth = iconSize + (iconHorizontalMargin / 2) + 1

export const cssSocialButtons = StyleSheet.create({
  loginBtn: {
    height: buttonHeight,
    paddingLeft: iconWidth,
    justifyContent: "center",
  },
  btnIcon: {
    position: 'absolute',
    fontSize: iconSize,
    height: iconSize,
    width: iconWidth,
    left: 0,
    top: (buttonHeight - iconSize) / 2,
    marginLeft: iconHorizontalMargin,
    paddingRight: (iconHorizontalMargin / 2) + 1,

    borderStyle: 'solid',
    borderRightColor: '#FFF',
    borderRightWidth: 0.4
  },
  btnText: {
    /*flex: 1,
    justifyContent: "center",*/
  },

  facebook: {
    backgroundColor: '#4C69BA',
  },
  google: {
    backgroundColor: '#DD4B39',
  }
})