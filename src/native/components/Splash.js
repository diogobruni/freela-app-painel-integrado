import React from 'react'
import PropTypes from 'prop-types'
import ReactNative, { Image, StyleSheet } from 'react-native'
//import { View } from 'native-base'
import { colors, device, commonStyles as cStyles } from '../styles'
import { Actions } from 'react-native-router-flux'
import Loading from './Loading'
import Header from './Header'

class Splash extends React.Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    getSessionUser: PropTypes.func.isRequired,
  }

  static defaultProps = {
    user: {},
  }

  constructor(props) {
    super(props)

    this.state = {
      sceneName: 'splash'
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.user.profile) {
      if (Actions.currentScene == this.state.sceneName) {
        Actions.dashboard()
      }
    } else {
      Actions.login()
    }
  }

  render() {
    // Customize Splash Screen
    return (
      <Loading />
    )
  }
}



const localStyles = StyleSheet.create({
})

export default Splash;
