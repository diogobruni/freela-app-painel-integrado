export default {
	// Loading
	loadingContent: 'Atualizando...',
	// Panels
	noPanelData: 'Nenhum dado encontrado com os parâmetros de filtro selecionados.',
}