export default {
  // Defaults
  default: 'Hmm, an unknown error occured',
  timeout: 'Server Timed Out. Check your internet connection',
  invalidJson: 'Response returned is not valid JSON',

  // Auth
  auth: 'Não foi possível efetuar o login, tente novamente.',

  // User
  userExists: 'Usuário já existe.',
  missingFirstName: 'First name is missing',
  missingLastName: 'Last name is missing',
  missingEmail: 'Email is missing',
  missingPassword: 'Password is missing',
  passwordsDontMatch: 'Passwords do not match',

  // Panels
  missingMealId: 'Missing meal definition',

  // Locale
  localeDoesNotExist: 'Sorry, we do not support that local',
};
