import { StyleSheet } from 'react-native'
import { cssColors, cssDevice } from './'

export const cssUtils = StyleSheet.create({
  block: {
    alignSelf: 'stretch',
  },
  vCenter: {
    alignItems: "center",
  },
  hCenter: {
    justifyContent: "center",
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
	padded: {
    paddingLeft: cssDevice.defaultPad,
    paddingRight: cssDevice.defaultPad,
  },
  noBorder: {
    borderWidth: 0,
    borderColor: 'transparent'
  },
  divider: {
    width: 80,
    height: 1,
  	borderTopColor: cssColors.main,
    borderStyle: 'solid',
    borderTopWidth: 0.4,
  },
  dividerWhite: {
  	borderTopColor: '#FFF',
  },
  dividerBlack: {
  	borderTopColor: '#000',
  },
  defaultTextStyle: {
    fontFamily: 'Roboto',
    fontWeight: '300',
    color: '#666'
  },
  textXSmallSize: {
  	fontSize: 10,
  },
  textSmallSize: {
  	fontSize: 12,
  },
  textNormalSize: {
  	fontSize: 14,
  },
  textXNormalSize: {
    fontSize: 16,
  },
  textBigSize: {
  	fontSize: 18,
  },
  textXBigSize: {
  	fontSize: 22,
  },
  textH1Size: {
  	fontSize: 27,
  },
  textH2Size: {
  	fontSize: 24,
  },
  textH3Size: {
  	fontSize: 21,
  },
})