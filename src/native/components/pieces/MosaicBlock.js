import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { Card, CardItem, Body, Text } from 'native-base'
import { skeleton } from '../../styles'

class MosaicBlock extends React.Component {
  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Card transparent style={localStyles.card}>
        <CardItem style={localStyles.cardItem}>
          <Body style={localStyles.cardBody}>
            {this.props.children}
          </Body>
        </CardItem>
      </Card>
    )
  }
}

const localStyles = StyleSheet.create({
  card: {},
  cardItem: {},
  cardBody: {
    alignItems: 'center'
  }
})

const mapStateToProps = state => ({
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(MosaicBlock)