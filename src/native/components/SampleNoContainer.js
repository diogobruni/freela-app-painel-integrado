import React from 'react'
import PropTypes from 'prop-types'
import ReactNative, { Image, StyleSheet } from 'react-native'

/* Import Modules */
import moment from 'moment'

/* Import Actions */
import { Actions } from 'react-native-router-flux'

/* Import UI Components */
import { Container, Header, Title, Content, Left, Body, Right, View, Text, Button, Icon, Item, Label, Card, CardItem, Grid, Col, Row, Picker, Form, H2 } from 'native-base'
import { translate } from '../../i18n'
import Loading from './Loading'
import LoadingOverlay from './LoadingOverlay'
import Messages from './Messages'
import MyDrawer from './MyDrawer'

/* Import Styles */
import { cssColors, cssDevice, cssUtils, cssSkeleton, cssStyles } from '../styles'

/* END OF IMPORTS */

/* DATA CONTROL */
/* END OF DATA CONTROL */

class PanelFacebookAds extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
  }

  static defaultProps = {
    error: null,
    locale: null,
    user: {},
  }

  constructor(props) {
    super(props)
  }

  render() {
    const { loading, error, locale } = this.props;

    //if (loading) return <Loading />;

    return (
      <Container>
        {loading && <LoadingOverlay />}
        <MyDrawer title="Bem vindo!">
          <Content padder>
            {error && <Messages message={error} />}
            
            <Text>Seja bem vindo!</Text>
          </Content>
        </MyDrawer>
      </Container>
    );
  }
}

const localStyles = StyleSheet.create({
})

export default PanelFacebookAds;
