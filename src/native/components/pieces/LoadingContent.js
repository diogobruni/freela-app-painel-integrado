import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { ActivityIndicator } from 'react-native'
import { View, Text } from 'native-base'
import { cssStyles, cssUtils } from '../../styles'
import Spacer from './Spacer'

/* Import Constants */
import messages from '../../../constants/messages'
/* /Import Constants */

class LoadingContent extends React.Component {
  static propTypes = {
  	text: PropTypes.string
  }

  static defaultProps = {}

  constructor(props) {
    super(props);
  }

  render() {
    const { text } = this.props

    return (
    	<View style={cssUtils.center}>
        <Spacer />
        <ActivityIndicator size="large" color="#777" />
        <Spacer />
        <Text style={cssStyles.mosaicSmallText}>{messages.loadingContent}</Text>
    	</View>
    )
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingContent)