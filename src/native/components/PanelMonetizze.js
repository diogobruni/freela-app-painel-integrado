import React from 'react'
import PropTypes from 'prop-types'
import ReactNative, { Image, StyleSheet } from 'react-native'

/* Import Modules */
// --

/* Import Constants */
import messages from '../../constants/messages'
/* /Import Constants */

/* Import Actions */
import { Actions } from 'react-native-router-flux'

/* Import UI Components */
import { Container, Header, Title, Content, Left, Body, Right, View, Text, Button, Icon, Item, Label, Card, CardItem, Grid, Col, Row, Picker, Form, H2 } from 'native-base'
import { translate } from '../../i18n'
import Loading from './Loading'
//import LoadingOverlay from './LoadingOverlay'
import LoadingContent from './pieces/LoadingContent'
import NoData from './pieces/NoData'
import Messages from './Messages'
import MyDrawer from './MyDrawer'
import DatePicker from 'react-native-datepicker'
import MosaicBlock from './pieces/MosaicBlock'
import Divider from './pieces/Divider'
import Spacer from './pieces/Spacer'

/* Import Styles */
import { cssColors, cssDevice, cssUtils, cssSkeleton, cssStyles } from '../styles'

/* END OF IMPORTS */
// --

/* DATA CONTROL */
import accounting from 'accounting'
import { getObjectValueOrDefault, getVarOrDefault } from '../../lib/renderAuxLib'
/* END OF DATA CONTROL */

class PanelMonetizze extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    filters: PropTypes.object,
    panels: PropTypes.object,
    setDateStart: PropTypes.func.isRequired,
    setDateEnd: PropTypes.func.isRequired,
    setMonetizzeAccount: PropTypes.func.isRequired,
    getPanelData: PropTypes.func.isRequired,
    getMonetizzeAccountList: PropTypes.func.isRequired,
  }

  static defaultProps = {
    error: null,
    locale: null,
    user: {},
  }

  constructor(props) {
    super(props)

    this.state = {
      sceneName: 'panelMonetizze',
      sceneTitle: 'Monetizze',
      panelId: 'monetizze'
    }

    this.handleChangeDateStart = this.handleChangeDateStart.bind(this)
    this.handleChangeDateEnd = this.handleChangeDateEnd.bind(this)
    this.handleChangeMonetizzeAccount = this.handleChangeMonetizzeAccount.bind(this)
  }

  handleChangeDateStart(value) {
    this.props.setDateStart(value, this.props.filters.dateEnd)
  }
  handleChangeDateEnd(value) {
    this.props.setDateEnd(this.props.filters.dateStart, value)
  }
  handleChangeMonetizzeAccount(value) {
    this.props.setMonetizzeAccount(value)
  }

  componentDidMount() {
    this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.monetizzeAccount)

    const interval = setInterval(function() {
      // React Native Timer Hack // Interval of 5 minutes
      let hackDate = new Date()
      if (hackDate.getMinutes() % 5 == 0) {
        if (Actions.currentScene == this.state.sceneName) {
          this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.monetizzeAccount)
        } else {
          clearInterval(interval)
        }
      }
    }.bind(this), 1 * 60 * 1000)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.filters.monetizzeAccount != this.props.filters.monetizzeAccount ||
      prevProps.filters.dateStart != this.props.filters.dateStart ||
      prevProps.filters.dateEnd != this.props.filters.dateEnd
    ) {
      this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.monetizzeAccount)
    }
  }

  renderAccountPicker(list) {
    let componentHtml = []
    if (list) {
      componentHtml = Object.keys(list).map(index => {
        const item = {
          id: index,
          name: list[index]
        }

        return (
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }

    return componentHtml
  }

  renderSalesValueChildren(data, maxCols) {
    const fields = [
      {
        key: 'cc_sales_value',
        placeholder: 'card'
      },
      {
        key: 'billets_sales_value',
        placeholder: 'barcode'
      },
    ]

    return this.renderChildrenCols('sales_value_', data, maxCols, fields)
  }

  renderSalesChildren(data, maxCols) {
    const fields = [
      {
        key: 'cc_sales',
        placeholder: 'card'
      },
      {
        key: 'billets_sales',
        placeholder: 'barcode'
      },
      {
        key: 'gratis_sales',
        placeholder: 'pricetag'
      },
    ]

    return this.renderChildrenCols('sales_', data, maxCols, fields)
  }

  renderCancelsChildren(data, maxCols) {
    const fields = [
      {
        key: 'cc_cancels',
        placeholder: 'card'
      },
      {
        key: 'billets_cancels',
        placeholder: 'barcode'
      },
      {
        key: 'gratis_cancels',
        placeholder: 'pricetag'
      },
    ]

    return this.renderChildrenCols('cancels_', data, maxCols, fields)
  }

  renderChildrenCols(type, data, maxCols, fields) {
    const childrenCols = (function() {
      let html = []

      if (data) {
        for (let field of fields) {
          if (data[field.key]) {
            const fieldData = data[field.key]
            html.push(
              <Col key={`${type}${field.key}`} style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                <Row style={cssStyles.mosaicRow}>
                  <MosaicBlock>
                    <Icon active name={field.placeholder} style={cssStyles.mosaicIcon} />
                    <Text style={cssStyles.mosaicMediumText}>{fieldData.formattedValue}</Text>
                  </MosaicBlock>
                </Row>
              </Col>
            )
          }
        }
      }

      return html
    })()

    const auxChildrenEmpty = (function(maxCols, iPrinted) {
      let html = []
      for (let i = iPrinted; i < maxCols; i++) {
        html.push(<Col key={`aux_${type}${i}`}></Col>)
      }
      return html
    })(maxCols, childrenCols.length)
    return [ childrenCols, auxChildrenEmpty ]
  }

  render() {
    const { loading, error, locale, filters, panels } = this.props;
    const { sceneTitle, panelId } = this.state
    let panelData = panels.panels[panelId] || false

    if (
      panelData &&
      panelData.sales_value &&
      panelData.sales_value.value == 0
    ) {
      panelData = false
    }

    /* FILTER PANEL */
    const pickerFilterAccount = jsonData ? this.renderAccountPicker(jsonData.dashboard.accounts) : []
    /* /FILTER PANEL */

    const maxSalesValueChildrenCols = 2
    const [ salesValueChildrenList, auxSalesValueChildrenCols ] = this.renderSalesValueChildren(panelData, maxSalesValueChildrenCols)

    const maxSalesChildrenCols = 3
    const [ salesChildrenList, auxSalesChildrenCols ] = this.renderSalesChildren(panelData, maxSalesChildrenCols)

    const maxCancelsChildrenCols = 3
    const [ cancelsChildrenList, auxCancelsChildrenCols ] = this.renderCancelsChildren(panelData, maxCancelsChildrenCols)

    //if (loading) return <Loading />
    return (
      <Container>
        <MyDrawer title={sceneTitle}>
          <Content padder>
            {error && <Messages message={error} />}
            
            <Card transparent>
              <CardItem header style={[cssUtils.center]}>
                <Text style={[cssUtils.defaultTextStyle, cssStyles.filterLabel]}>Selecione o período e a conta desejada</Text>
              </CardItem>
              <CardItem>
                <Body style={[cssUtils.center]}>
                  <Form>
                    <Item style={[cssUtils.noBorder, cssUtils.center, { marginLeft: 0 }]}>
                      <DatePicker
                        //style={{width: 200}}
                        date={filters.dateStart}
                        mode="date"
                        placeholder="Selecione a data inicial"
                        format="DD/MM/YYYY"
                        //minDate="2016-05-01"
                        //maxDate="2016-06-01"
                        confirmBtnText="Confirmar"
                        cancelBtnText="Cancelar"
                        style={datePickerStyle}
                        customStyles={datePickerExtraStyle}
                        onDateChange={this.handleChangeDateStart}
                      />
                      <Text style={[cssUtils.defaultTextStyle, cssStyles.datepickerSeparatorText]}>
                        até
                      </Text>
                      <DatePicker
                        //style={{width: 200}}
                        date={filters.dateEnd}
                        mode="date"
                        placeholder="Selecione a data final"
                        format="DD/MM/YYYY"
                        //minDate="2016-05-01"
                        //maxDate="2016-06-01"
                        confirmBtnText="Confirmar"
                        cancelBtnText="Cancelar"
                        style={datePickerStyle}
                        customStyles={datePickerExtraStyle}
                        onDateChange={this.handleChangeDateEnd}
                      />
                    </Item>
                    <Picker
                      iosHeader="Select one"
                      mode="dropdown"
                      selectedValue={filters.monetizzeAccount}
                      onValueChange={this.handleChangeMonetizzeAccount}
                      style={[cssStyles.dropdownPicker, {marginTop: 10}]}
                    >
                      <Picker.Item label="Todas as contas" value="all" />
                      {pickerFilterAccount}
                    </Picker>
                  </Form>
                </Body>
              </CardItem>
            </Card>

            {loading &&
              <LoadingContent />
            }

            {panelData && 
              <View>
                <H2 style={cssStyles.sectionTitle}>Estatísticas</H2>
                
                <MosaicBlock>
                  <Text style={cssStyles.mosaicBigText}>
                    {getObjectValueOrDefault(panelData, 'sales_value')}
                  </Text>
                  <Text style={[cssStyles.mosaicSmallText]}>Receita</Text>
                </MosaicBlock>
                <Grid>
                  {salesValueChildrenList}
                  {auxSalesValueChildrenCols}
                </Grid>

                <Spacer />

                <MosaicBlock>
                  <Text style={cssStyles.mosaicBigText}>
                    {getObjectValueOrDefault(panelData, 'sales')}
                  </Text>
                  <Text style={[cssStyles.mosaicSmallText]}>Vendas</Text>
                </MosaicBlock>
                <Grid>
                  {salesChildrenList}
                  {auxSalesChildrenCols}
                </Grid>

                <Spacer />

                <Grid>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'refunds')}
                        </Text>
                        <Text style={[cssStyles.mosaicSmallText]}>Reembolso</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'billets_pending')}
                        </Text>
                        <Text style={[cssStyles.mosaicSmallText]}>Boletos Impressos</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                </Grid>

                <Spacer />

                <MosaicBlock>
                  <Text style={cssStyles.mosaicBigText}>
                    {getObjectValueOrDefault(panelData, 'cancels')}
                  </Text>
                  <Text style={[cssStyles.mosaicSmallText]}>Vendas Canceladas</Text>
                </MosaicBlock>
                <Grid>
                  {cancelsChildrenList}
                  {auxCancelsChildrenCols}
                </Grid>
              </View>
            }
            {!loading && !panelData &&
              <NoData text={messages.noPanelData} />
            }
          </Content>
        </MyDrawer>
      </Container>
    );
  }
}

const datePickerStyle = {
  //width: 130
  alignSelf: 'stretch'
}
const datePickerExtraStyle = {
  dateIcon: {
    display: 'none',
  },
  dateInput: {
    marginLeft: 0,
    borderWidth: 0,
    backgroundColor: cssColors.main,
  },
  dateText: {
    color: '#FFF'
  },
  placeholderText: {
    color: '#FFF'
  }
}

const localStyles = StyleSheet.create({
})

export default PanelMonetizze
