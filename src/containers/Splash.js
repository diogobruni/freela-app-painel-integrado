import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

//import { login, getMemberData } from '../actions/auth';
import { getSessionUser } from '../actions/user'

class Splash extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    getSessionUser: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.getSessionUser()
  }

  render = () => {
    const {
      Layout,
      user,
      getSessionUser,
    } = this.props;

    return <Layout
      user={user}
      getSessionUser={getSessionUser}
    />;
  }
}

const mapStateToProps = state => ({
  user: state.user || {},
})

const mapDispatchToProps = {
  getSessionUser,
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash)