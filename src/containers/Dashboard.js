import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setDateStart, setDateEnd, setPanelId, getCustomPanelList } from '../actions/filters'
import { getPanelData } from '../actions/panels'

class Dashboard extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    filters: PropTypes.object,
    panels: PropTypes.object,
    setDateStart: PropTypes.func.isRequired,
    setDateEnd: PropTypes.func.isRequired,
    setPanelId: PropTypes.func.isRequired,
    getPanelData: PropTypes.func.isRequired,
    getCustomPanelList: PropTypes.func.isRequired,
  }

  render = () => {
    const {
      Layout,
      locale,
      user,
      loading,
      info,
      error,
      success,
      filters,
      panels,
      setDateStart,
      setDateEnd,
      setPanelId,
      getPanelData,
      getCustomPanelList,
    } = this.props;

    return <Layout
      locale={locale}
      user={user}
      loading={loading}
      info={info}
      error={error}
      success={success}
      filters={filters}
      panels={panels}
      setDateStart={setDateStart}
      setDateEnd={setDateEnd}
      setPanelId={setPanelId}
      getPanelData={getPanelData}
      getCustomPanelList={getCustomPanelList}
    />;
  }
}

const mapStateToProps = state => ({
  locale: state.locale || null,
  user: state.user || {},
  loading: state.status.loading || false,
  info: state.status.info || null,
  error: state.status.error || null,
  success: state.status.success || null,
  filters: state.filters || {},
  panels: state.panels || {},
})

const mapDispatchToProps = {
  setDateStart,
  setDateEnd,
  setPanelId,
  getPanelData,
  getCustomPanelList,
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)