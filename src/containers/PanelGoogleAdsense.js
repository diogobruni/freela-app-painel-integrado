import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setDateStart, setDateEnd, setGoogleAccount, getGoogleAccountList } from '../actions/filters'
import { getPanelData } from '../actions/panels'

class PanelGoogleAdsense extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    filters: PropTypes.object,
    panels: PropTypes.object,
    setDateStart: PropTypes.func.isRequired,
    setDateEnd: PropTypes.func.isRequired,
    setGoogleAccount: PropTypes.func.isRequired,
    getPanelData: PropTypes.func.isRequired,
    getGoogleAccountList: PropTypes.func.isRequired,
  }

  //componentDidMount = () => this.props.getMemberData();

  render = () => {
    const {
      Layout,
      locale,
      user,
      loading,
      info,
      error,
      success,
      filters,
      panels,
      setDateStart,
      setDateEnd,
      setGoogleAccount,
      getPanelData,
      getGoogleAccountList,
    } = this.props;

    return <Layout
      locale={locale}
      user={user}
      loading={loading}
      info={info}
      error={error}
      success={success}
      filters={filters}
      panels={panels}
      setDateStart={setDateStart}
      setDateEnd={setDateEnd}
      setGoogleAccount={setGoogleAccount}
      getPanelData={getPanelData}
      getGoogleAccountList={getGoogleAccountList}
    />;
  }
}

const mapStateToProps = state => ({
  locale: state.locale || null,
  user: state.user || {},
  loading: state.status.loading || false,
  info: state.status.info || null,
  error: state.status.error || null,
  success: state.status.success || null,
  filters: state.filters || {},
  panels: state.panels || {},
})

const mapDispatchToProps = {
  setDateStart,
  setDateEnd,
  setGoogleAccount,
  getPanelData,
  getGoogleAccountList,
}

export default connect(mapStateToProps, mapDispatchToProps)(PanelGoogleAdsense)