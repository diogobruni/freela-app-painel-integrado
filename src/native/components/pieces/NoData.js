import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { StyleSheet } from 'react-native'
import { View, Text, Icon } from 'native-base'
import { skeleton, cssStyles } from '../../styles'
import MosaicBlock from './MosaicBlock'
import Spacer from './Spacer'

class NoData extends React.Component {
  static propTypes = {
  	text: PropTypes.string
  }

  static defaultProps = {}

  constructor(props) {
    super(props);
  }

  render() {
    const { text } = this.props

    return (
    	<View>
    		<MosaicBlock>
    			<Icon name="alert" style={cssStyles.mosaicBigIcon}></Icon>
    			<Spacer size={10} />
		      <Text style={[cssStyles.mosaicSmallText]}>
		      	{text}
	      	</Text>
		    </MosaicBlock>
    	</View>
    )
  }
}

const localStyles = StyleSheet.create({
})

const mapStateToProps = state => ({
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(NoData)