import React from 'react';
import PropTypes from 'prop-types';
import { Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { cssStyles } from '../styles'

class Navbar extends React.Component {
  static propTypes = {
    title: PropTypes.string,
    openDrawer: PropTypes.func,
  }

  static defaultProps = {
    title: 'Painel Integrado',
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { title, openDrawer } = this.props

    return (
      <Header style={cssStyles.header}>
        {openDrawer &&
          <Left>
            <Button
              transparent
              onPress={openDrawer}
            >
              <Icon name="menu" />
            </Button>
          </Left>
        }
        {title &&
          <Body>
            <Title>{title}</Title>
          </Body>
        }
      </Header>
    )
  }
}

export default Navbar