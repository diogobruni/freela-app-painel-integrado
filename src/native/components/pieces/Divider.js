import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import { cssUtils } from '../../styles'

class Divider extends React.Component {
  static propTypes = {
    color: PropTypes.string
  }

  static defaultProps = {
    color: '#FFF',
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { color } = this.props

    return (
      <View style={[localStyles.divider, { borderTopColor: color }]}></View>
    )
  }
}

const localStyles = StyleSheet.create({
  divider: {
    alignSelf: 'stretch',
    height: 1,
    borderTopColor: '#FFF',
    borderStyle: 'solid',
    borderTopWidth: 0.4,
    marginTop: 15,
    marginBottom: 15
  }
})

const mapStateToProps = state => ({
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Divider)