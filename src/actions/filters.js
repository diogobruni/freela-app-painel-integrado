import moment from 'moment'
import PainelIntegrado from '../lib/painel-integrado'

export function setDateStart(dateStart, dateEnd) {
  return dispatch => new Promise((resolve, reject) => {
    const mStartDate = moment(dateStart, 'DD/MM/YYYY')
    const mUntilDate = moment(dateEnd, 'DD/MM/YYYY')

    if (mUntilDate.isBefore(mStartDate)) {
      dispatch({
        type: 'DATE_END_CHANGE',
        data: dateStart
      })
    }

    return resolve(dispatch({
      type: 'DATE_START_CHANGE',
      data: dateStart
    }))
  })
}

export function setDateEnd(dateStart, dateEnd) {
  return dispatch => new Promise((resolve, reject) => {
    const mStartDate = moment(dateStart, 'DD/MM/YYYY')
    const mUntilDate = moment(dateEnd, 'DD/MM/YYYY')

    if (mStartDate.isAfter(mUntilDate)) {
      dispatch({
        type: 'DATE_START_CHANGE',
        data: dateEnd
      })
    }

    return resolve(dispatch({
      type: 'DATE_END_CHANGE',
      data: dateEnd
    }))
  })
}

export function setPanelId(val) {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'PANEL_ID_CHANGE',
      data: val
    }))
  })
}

export function setFBAccount(val) {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'FB_ACCOUNT_CHANGE',
      data: val
    }))
  })
}

export function setGoogleAccount(val) {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'GOOGLE_ACCOUNT_CHANGE',
      data: val
    }))
  })
}

export function setMonetizzeAccount(val) {
  return dispatch => new Promise((resolve, reject) => {
    return resolve(dispatch({
      type: 'MONETIZZE_ACCOUNT_CHANGE',
      data: val
    }))
  })
}

export function getCustomPanelList() {
  return dispatch => new Promise(async (resolve, reject) => {
    try {
      const { response, json } = await PainelIntegrado.getPanelList()

      if (response.ok) {
        const customPanelList = []
        for(let i in json) {
          const panel = json[i]
          if (panel.type == 1) {
            customPanelList.push(panel)
          }
        }

        resolve(dispatch({
          type: 'UPDATE_CUSTOM_PANEL_LIST',
          data: customPanelList
        }))
      } else {
        return resolve(await statusMessage(dispatch, 'error', 'Não foi possível coletar os dados de paineis, tentarei novamente em breve, aguarde por favor.'))
      }
    } catch(err) {}
  })
}