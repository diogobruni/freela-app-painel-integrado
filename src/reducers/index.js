import status from './status';
import user from './user';
import locale from './locale';
import drawer from './drawer';
import filters from './filters';
import panels from './panels';

const rehydrated = (state = false, action) => {
  switch (action.type) {
    case 'persist/REHYDRATE':
      return true;
    default:
      return state;
  }
};

export default {
  rehydrated,
  status,
  user,
  locale,
  drawer,
  filters,
  panels,
};
