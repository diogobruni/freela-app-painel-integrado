import Store from '../store/user'

export const initialState = Store

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case 'SOCIAL_LOGIN': {
      if (action.data) {
        return {
          ...state,
          token: action.data.token
        }
      }
      return initialState
    }
    case 'PASSWORD_LOGIN': {
      if (action.data) {
        return {
          ...state,
          token: action.data.token
        }
      }
      return initialState
    }
    case 'SOCIAL_LOGIN_FAIL': {
      return initialState
    }
    case 'GET_SESSION_USER': {
      if (action.data) {
        return {
          ...state,
          profile: action.data,
        }
      }
      return initialState
    }
    case 'GET_SESSION_USER_FAIL': {
      if (action.data) {
        return {
          ...state,
        }
      }
      return initialState
    }
    case 'USER_RESET': {
      return initialState
    }
    default:
      return state
  }
}
