import { cssColors } from './'

export const cssStyles = {
	header: {
		backgroundColor: cssColors.main
	},
	section: {},
	sectionTitle: {
		fontFamily: 'Roboto_light',
		fontWeight: '100',
		color: '#666',
		fontSize: 20,
		marginTop: 20,
		marginBottom: 4,
	},

	boxDateRangePicker: {},
	filterLabel: {
    fontSize: 14,
  },
  dropdownPicker: {
    color: '#666'
  },
  datepickerLabel: {
    marginTop: 10,
    marginBottom: 6
  },
  datepickerSeparatorText: {
    borderTopWidth: 0,
    fontSize: 14,
    paddingLeft: 10,
    paddingRight: 10
  },
  mosaicRow: {},
  mosaicBigText: {
    fontFamily: 'Roboto_light',
    fontWeight: '100',
    fontSize: 23,
    color: '#666'
  },
  mosaicMediumText: {
    fontFamily: 'Roboto_light',
    fontWeight: '100',
    fontSize: 16,
    color: '#666'
  },
  mosaicSmallText: {
    fontFamily: 'Roboto_light',
    fontWeight: '100',
    fontSize: 13,
    color: '#666'
  },
  mosaicImageIcon: {
    position: 'absolute',
    left: 0,
    width: 20,
    height: 20
  },
  mosaicBigIcon: {
    color: '#777',
    fontSize: 42,
  },
  textProfit: {
    color: '#1c84c6'
  },
  textWaste: {
    color: '#ed5565'
  },
}