const devMode = (['dev', 'development'].includes(process.env.NODE_ENV))

export const Facebook = {
	app: {
		id: devMode ? '1680568298665410' : '1680568298665410',
		scopes: ['public_profile', 'email'],
		fields: 'name,email'
	}
}

export const Google = {
	app: {
		androidClientId: devMode ? '240883573729-map64lmg7to778cdtso710lu5cki0i1k.apps.googleusercontent.com' : '240883573729-map64lmg7to778cdtso710lu5cki0i1k.apps.googleusercontent.com',
		iosClientId: devMode ? '240883573729-mv3erv7seqjqq68geltjuj5sefd2m56u.apps.googleusercontent.com' : '240883573729-mv3erv7seqjqq68geltjuj5sefd2m56u.apps.googleusercontent.com',
		scopes: ['profile', 'email'],
	}
}