import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import FadeInView from 'react-native-fade-in-view'
//import Colors from '../../../native-base-theme/variables/commonColor';

const fadeInViewStyle = {
	flex: 1,
	flexDirection: 'column',
	justifyContent: 'center',
	position: 'absolute',
	top: 0,
	left: 0,
	right: 0,
	bottom: 0,
}

const insideViewStyle = {
	backgroundColor: '#000',
	opacity: 0.7
}

const LoadingOverlay = () => (
  <FadeInView duration={300} style={[{ zIndex: 10 }, fadeInViewStyle]}>
  	<View style={[fadeInViewStyle, insideViewStyle]}>
    	<ActivityIndicator size="large" color="#FFF" />
  	</View>
  </FadeInView>
);

export default LoadingOverlay;
