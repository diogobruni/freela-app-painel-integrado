import ErrorMessages from '../constants/errors'
import statusMessage from './status'
import PainelIntegrado from '../lib/painel-integrado'

//import { Toast } from 'native-base';

//export function getPanelData(panelId = 'dashboard', startDate = false, untilDate = false) {
export function getPanelData(params = {}) {
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			params.panelId = params.panelId || 'dashboard'

			await statusMessage(dispatch, 'loading', true)

			let formattedJSON = {}
			const { response, json } = await PainelIntegrado.getPanelData(params)

			if (response.ok) {
				formattedJSON = PainelIntegrado.formatPanelData(json)
			}
			await statusMessage(dispatch, 'loading', false)

			if (response.ok) {
				return resolve(dispatch({
					type: 'UPDATE_PANEL_DATA',
					data: {
						panelId: params.panelId,
						panel: formattedJSON
					}
				}))
			} else {
				return resolve(await statusMessage(dispatch, 'error', 'Falha na conexão com a API de Painel.'))
			}
		} catch(err) {
			throw err
			return reject(err)
		}
	})
}

/*export function filterPanel(xhrData, startDate, untilDate, panelId = 'dashboard', account = 'all') {
	return dispatch => new Promise(async (resolve, reject) => {
		try {
			const panelData = xhrData.panels[panelId] || false
			if (panelData) {
				const dealData = PainelIntegrado.dealPanelJSON(panelData, startDate, untilDate, panelId, account)
				return resolve(dispatch({
					type: 'UPDATE_PANEL_DATA',
					data: dealData
				}))
			} else {
				return resolve(await statusMessage(dispatch, 'error', 'Painel sem dados.'))
			}
		} catch(err) {
			throw err
			return resolve(await statusMessage(dispatch, 'error', 'Não foi possível filtrar os dados do painel.'))
		}
	})
}*/