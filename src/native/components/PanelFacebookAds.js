import React from 'react'
import PropTypes from 'prop-types'
import ReactNative, { Image, StyleSheet } from 'react-native'

/* Import Modules */
// --

/* Import Constants */
import messages from '../../constants/messages'
/* /Import Constants */

/* Import Actions */
import { Actions } from 'react-native-router-flux'

/* Import UI Components */
import { Container, Header, Title, Content, Left, Body, Right, View, Text, Button, Icon, Item, Label, Card, CardItem, Grid, Col, Row, Picker, Form, H2 } from 'native-base'
import { translate } from '../../i18n'
import Loading from './Loading'
//import LoadingOverlay from './LoadingOverlay'
import LoadingContent from './pieces/LoadingContent'
import NoData from './pieces/NoData'
import Messages from './Messages'
import MyDrawer from './MyDrawer'
import DatePicker from 'react-native-datepicker'
import MosaicBlock from './pieces/MosaicBlock'
import Divider from './pieces/Divider'

/* Import Styles */
import { cssColors, cssDevice, cssUtils, cssSkeleton, cssStyles } from '../styles'

/* END OF IMPORTS */
// --

/* DATA CONTROL */
import accounting from 'accounting'
import { getObjectValueOrDefault, getVarOrDefault } from '../../lib/renderAuxLib'
/* END OF DATA CONTROL */

class PanelFacebookAds extends React.Component {
  static propTypes = {
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    filters: PropTypes.object,
    panels: PropTypes.object,
    setDateStart: PropTypes.func.isRequired,
    setDateEnd: PropTypes.func.isRequired,
    setFBAccount: PropTypes.func.isRequired,
    getPanelData: PropTypes.func.isRequired,
    getFBAccountList: PropTypes.func.isRequired,
  }

  static defaultProps = {
    error: null,
    locale: null,
    user: {},
  }

  constructor(props) {
    super(props)

    this.state = {
      sceneName: 'panelFacebookAds',
      sceneTitle: 'Facebook Ads',
      panelId: 'facebook_ads'
    }

    this.handleChangeDateStart = this.handleChangeDateStart.bind(this)
    this.handleChangeDateEnd = this.handleChangeDateEnd.bind(this)
    this.handleChangeFBAccount = this.handleChangeFBAccount.bind(this)
  }

  handleChangeDateStart(value) {
    this.props.setDateStart(value, this.props.filters.dateEnd)
  }
  handleChangeDateEnd(value) {
    this.props.setDateEnd(this.props.filters.dateStart, value)
  }
  handleChangeFBAccount(value) {
    this.props.setFBAccount(value)
  }

  manageGetPanelData(panelId, dateStart, dateEnd, fbAccount) {
    const params = {
      panelId,
      dateStart,
      dateEnd,
      fbAccount,
    }
    return this.props.getPanelData(params)
  }

  componentDidMount() {
    this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.fbAccount)

    const interval = setInterval(function() {
      // React Native Timer Hack // Interval of 5 minutes
      let hackDate = new Date()
      if (hackDate.getMinutes() % 5 == 0) {
        if (Actions.currentScene == this.state.sceneName) {
          this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.fbAccount)
        } else {
          clearInterval(interval)
        }
      }
    }.bind(this), 1 * 60 * 1000)
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.filters.fbAccount != this.props.filters.fbAccount ||
      prevProps.filters.dateStart != this.props.filters.dateStart ||
      prevProps.filters.dateEnd != this.props.filters.dateEnd
    ) {
      this.manageGetPanelData(this.state.panelId, this.props.filters.dateStart, this.props.filters.dateEnd, this.props.filters.fbAccount)
    }
  }

  renderAccountPicker(list) {
    let componentHtml = []
    if (list) {
      componentHtml = Object.keys(list).map(index => {
        const item = {
          id: index,
          name: list[index]
        }

        return (
          <Picker.Item key={item.id} label={item.name} value={item.id} />
        )
      })
    }

    return componentHtml
  }

  render() {
    const { loading, error, locale, filters, panels } = this.props
    const { sceneTitle, panelId } = this.state
    let panelData = panels.panels[panelId] || false

    if (
      panelData &&
      panelData.spend &&
      panelData.spend.value == 0
    ) {
      panelData = false
    }

    /* FILTER PANEL */
    const pickerFilterAccount = jsonData ? this.renderAccountPicker(jsonData.dashboard.accounts) : []
    /* /FILTER PANEL */

    // GAMB FORMAT VALUES
    const fieldsToReformat = [
      'spend',
      'cpm',
      'clicks_cpc',
      'cpc_purchase',
    ]
    for(let field of fieldsToReformat) {
      if (panelData[field]) {
        panelData[field].formattedValue = accounting.formatMoney(panelData[field].value / 100, "R$ ", 2, ".", ",")
      }
    }
    // /GAMB FORMAT VALUES

    //if (loading) return <Loading />
    return (
      <Container>
        <MyDrawer title={sceneTitle}>
          <Content padder>
            {error && <Messages message={error} />}
            
            <Card transparent>
              <CardItem header style={[cssUtils.center]}>
                <Text style={[cssUtils.defaultTextStyle, cssStyles.filterLabel]}>Selecione o período e a conta desejada</Text>
              </CardItem>
              <CardItem>
                <Body style={[cssUtils.center]}>
                  <Form>
                    <Item style={[cssUtils.noBorder, cssUtils.center, { marginLeft: 0 }]}>
                      <DatePicker
                        //style={{width: 200}}
                        date={filters.dateStart}
                        mode="date"
                        placeholder="Selecione a data inicial"
                        format="DD/MM/YYYY"
                        //minDate="2016-05-01"
                        //maxDate="2016-06-01"
                        confirmBtnText="Confirmar"
                        cancelBtnText="Cancelar"
                        style={datePickerStyle}
                        customStyles={datePickerExtraStyle}
                        onDateChange={this.handleChangeDateStart}
                      />
                      <Text style={[cssUtils.defaultTextStyle, cssStyles.datepickerSeparatorText]}>
                        até
                      </Text>
                      <DatePicker
                        //style={{width: 200}}
                        date={filters.dateEnd}
                        mode="date"
                        placeholder="Selecione a data final"
                        format="DD/MM/YYYY"
                        //minDate="2016-05-01"
                        //maxDate="2016-06-01"
                        confirmBtnText="Confirmar"
                        cancelBtnText="Cancelar"
                        style={datePickerStyle}
                        customStyles={datePickerExtraStyle}
                        onDateChange={this.handleChangeDateEnd}
                      />
                    </Item>
                    <Picker
                      iosHeader="Select one"
                      mode="dropdown"
                      selectedValue={filters.fbAccount}
                      onValueChange={this.handleChangeFBAccount}
                      style={[cssStyles.dropdownPicker, {marginTop: 10}]}
                    >
                      <Picker.Item label="Todas as contas" value="all" />
                      {pickerFilterAccount}
                    </Picker>
                  </Form>
                </Body>
              </CardItem>
            </Card>

            {loading &&
              <LoadingContent />
            }

            {panelData && 
              <View>
                <H2 style={cssStyles.sectionTitle}>Resultados</H2>
                <Grid>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'spend')}
                        </Text>
                        <Text style={[cssStyles.mosaicSmallText]}>Investimento</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'impressions')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>Impressões</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                </Grid>

                <Grid>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'cpm')}
                        </Text>
                        <Text style={[cssStyles.mosaicSmallText]}>CPM</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicBigText}>
                          {getObjectValueOrDefault(panelData, 'reach')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>Alcance</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                </Grid>

                <H2 style={cssStyles.sectionTitle}>Estatísticas</H2>
                <MosaicBlock>
                  <Text style={cssStyles.mosaicBigText}>
                    {getObjectValueOrDefault(panelData, 'clicks')}
                  </Text>
                  <Text style={[cssStyles.mosaicSmallText]}>Cliques</Text>
                </MosaicBlock>
                <Grid>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicMediumText}>
                          {getObjectValueOrDefault(panelData, 'clicks_ctr')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>CTR</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicMediumText}>
                          {getObjectValueOrDefault(panelData, 'clicks_cpc')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>CPC</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                </Grid>

                <H2 style={cssStyles.sectionTitle}>Conversão</H2>
                <MosaicBlock>
                  <Text style={cssStyles.mosaicBigText}>
                    {getObjectValueOrDefault(panelData, 'conversion_purchases')}
                  </Text>
                  <Text style={[cssStyles.mosaicSmallText]}>Compras</Text>
                </MosaicBlock>
                <Grid>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicMediumText}>
                          {getObjectValueOrDefault(panelData, 'cpc_purchase')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>Custo por Compra</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                  <Col style={[cssSkeleton.col, cssSkeleton.firstCol]}>
                    <Row style={cssStyles.mosaicRow}>
                      <MosaicBlock>
                        <Text style={cssStyles.mosaicMediumText}>
                          {getObjectValueOrDefault(panelData, 'conversion_rate')}
                        </Text>
                        <Text style={cssStyles.mosaicSmallText}>Taxa de Conversão</Text>
                      </MosaicBlock>
                    </Row>
                  </Col>
                </Grid>
              </View>
            }
            {!loading && !panelData &&
              <NoData text={messages.noPanelData} />
            }
          </Content>
        </MyDrawer>
      </Container>
    );
  }
}

const datePickerStyle = {
  //width: 130
  alignSelf: 'stretch'
}
const datePickerExtraStyle = {
  dateIcon: {
    display: 'none',
  },
  dateInput: {
    marginLeft: 0,
    borderWidth: 0,
    backgroundColor: cssColors.main,
  },
  dateText: {
    color: '#FFF'
  },
  placeholderText: {
    color: '#FFF'
  }
}

const localStyles = StyleSheet.create({
})

export default PanelFacebookAds
