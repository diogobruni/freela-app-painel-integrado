import Store from '../store/panels'

export const initialState = Store

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_PANEL_DATA': {
      if (action.data) {
        return {
          ...state,
          panels: {
            ...state.panels,
            [action.data.panelId]: action.data.panel
          },
        }
      }
      return initialState
    }
    case 'UPDATE_PANELS': {
      if (action.data) {
        return {
          ...state,
          ...action.data
        }
      }
      return initialState
    }
    case 'RESET_PANELS': {
      return initialState
    }
    default:
      return state;
  }
}
