export const initialState = {
  state: 'closed',
};

export default function drawerReducer(state = initialState, action) {
  switch (action.type) {
    case 'CLOSE_DRAWER': {
      return {
        ...state,
        state: 'closed',
        generatedTimestamp: action.data.generatedTimestamp
      };
    }
    case 'OPEN_DRAWER': {
      return {
        ...state,
        state: 'opened',
        generatedTimestamp: action.data.generatedTimestamp
      };
    }
    default:
      return state;
  }
}
