import ReactNative from 'react-native'
const { Dimensions, Platform } = ReactNative
const deviceHeight = Dimensions.get("window").height
const deviceWidth = Dimensions.get("window").width

export const cssDevice = {
	height: deviceHeight,
	width: deviceWidth,
	platform: Platform,
	defaultPad: deviceWidth / 10
}