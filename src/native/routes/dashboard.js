import React from 'react'
import { Scene, Stack } from 'react-native-router-flux'
import { Icon } from 'native-base'

import DefaultProps from '../constants/navigation'
import AppConfig from '../../constants/config'

import DashboardContainer from '../../containers/Dashboard'
import DashboardComponent from '../components/Dashboard'

import PanelFacebookAdsContainer from '../../containers/PanelFacebookAds'
import PanelFacebookAdsComponent from '../components/PanelFacebookAds'

import PanelGoogleAdsenseContainer from '../../containers/PanelGoogleAdsense'
import PanelGoogleAdsenseComponent from '../components/PanelGoogleAdsense'

import PanelMonetizzeContainer from '../../containers/PanelMonetizze'
import PanelMonetizzeComponent from '../components/PanelMonetizze'

import { ActionConst } from 'react-native-router-flux'

const Dashboard = (
  <Stack>
    <Scene
      hideNavBar
      key="dashboard"
      type={ActionConst.REPLACE}
      {...DefaultProps.navbarProps}
      component={DashboardContainer}
      Layout={DashboardComponent}
    />

    <Scene
      hideNavBar
      key="panelFacebookAds"
      type={ActionConst.REPLACE}
      {...DefaultProps.navbarProps}
      component={PanelFacebookAdsContainer}
      Layout={PanelFacebookAdsComponent}
    />

    <Scene
      hideNavBar
      key="panelGoogleAdsense"
      type={ActionConst.REPLACE}
      {...DefaultProps.navbarProps}
      component={PanelGoogleAdsenseContainer}
      Layout={PanelGoogleAdsenseComponent}
    />

    <Scene
      hideNavBar
      key="panelMonetizze"
      type={ActionConst.REPLACE}
      {...DefaultProps.navbarProps}
      component={PanelMonetizzeContainer}
      Layout={PanelMonetizzeComponent}
    />
  </Stack>
)

export default Dashboard
