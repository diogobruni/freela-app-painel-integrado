import Store from '../store/filters'

export const initialState = Store

export default function filterReducer(state = initialState, action) {
  switch (action.type) {
    case 'DATE_START_CHANGE': {
      if (action.data) {
        return {
          ...state,
          dateStart: action.data
        }
      }
      return initialState
    }
    case 'DATE_END_CHANGE': {
      if (action.data) {
        return {
          ...state,
          dateEnd: action.data
        }
      }
      return initialState
    }
    case 'PANEL_ID_CHANGE': {
      if (action.data) {
        return {
          ...state,
          panelId: action.data
        }
      }
      return initialState
    }
    case 'FB_ACCOUNT_CHANGE': {
      if (action.data) {
        return {
          ...state,
          fbAccount: action.data
        }
      }
      return initialState
    }
    case 'GOOGLE_ACCOUNT_CHANGE': {
      if (action.data) {
        return {
          ...state,
          googleAccount: action.data
        }
      }
      return initialState
    }
    case 'MONETIZZE_ACCOUNT_CHANGE': {
      if (action.data) {
        return {
          ...state,
          monetizzeAccount: action.data
        }
      }
      return initialState
    }
    case 'UPDATE_CUSTOM_PANEL_LIST': {
      if (action.data) {
        return {
          ...state,
          customPanelList: action.data
        }
      }
      return initialState
    }
    default:
      return state;
  }
}
