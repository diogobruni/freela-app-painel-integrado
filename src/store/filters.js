import moment from 'moment'
const todayDate = moment().format('DD/MM/YYYY')

export default {
  dateStart: todayDate,
  dateEnd: todayDate,
  panelId: 'dashboard',
  fbAccount: 'all',
  googleAccount: 'all',
  monetizzeAccount: 'all',
}