import { StyleSheet } from 'react-native'

export const cssSkeleton = StyleSheet.create({
	col: {
		paddingLeft: 3,
    paddingRight: 3
	},
	firstCol: {
		paddingLeft: 0
	},
	lastCol: {
		paddingRight: 0
	},
})