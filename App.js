console.ignoredYellowBox = [ 'Setting a timer' ]

import React from 'react'
import Root from './src/native/index'
import configureStore from './src/store/index'

// -------- SENTRY
import Sentry from 'sentry-expo'
// Remove this once Sentry is correctly setup.
Sentry.enableInExpoDevelopment = true
Sentry.config('https://376fe2de103c4d1f9771ce26d8c18ac7@sentry.io/1246570').install()

const { persistor, store } = configureStore()

export default function App() {
  return <Root store={store} persistor={persistor} />
}
