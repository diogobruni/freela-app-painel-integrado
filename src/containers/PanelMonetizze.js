import React,{ Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setDateStart, setDateEnd, setMonetizzeAccount, getMonetizzeAccountList } from '../actions/filters'
import { getPanelData } from '../actions/panels'

class PanelMonetizze extends Component {
  static propTypes = {
    Layout: PropTypes.func.isRequired,
    locale: PropTypes.string,
    user: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    info: PropTypes.string,
    error: PropTypes.string,
    success: PropTypes.string,
    filters: PropTypes.object,
    panels: PropTypes.object,
    setDateStart: PropTypes.func.isRequired,
    setDateEnd: PropTypes.func.isRequired,
    setMonetizzeAccount: PropTypes.func.isRequired,
    getPanelData: PropTypes.func.isRequired,
    getMonetizzeAccountList: PropTypes.func.isRequired,
  }

  //componentDidMount = () => this.props.getMemberData();

  render = () => {
    const {
      Layout,
      locale,
      user,
      loading,
      info,
      error,
      success,
      filters,
      panels,
      setDateStart,
      setDateEnd,
      setMonetizzeAccount,
      getPanelData,
      getMonetizzeAccountList,
    } = this.props;

    return <Layout
      locale={locale}
      user={user}
      loading={loading}
      info={info}
      error={error}
      success={success}
      filters={filters}
      panels={panels}
      setDateStart={setDateStart}
      setDateEnd={setDateEnd}
      setMonetizzeAccount={setMonetizzeAccount}
      getPanelData={getPanelData}
      getMonetizzeAccountList={getMonetizzeAccountList}
    />;
  }
}

const mapStateToProps = state => ({
  locale: state.locale || null,
  user: state.user || {},
  loading: state.status.loading || false,
  info: state.status.info || null,
  error: state.status.error || null,
  success: state.status.success || null,
  filters: state.filters || {},
  panels: state.panels || {},
})

const mapDispatchToProps = {
  setDateStart,
  setDateEnd,
  setMonetizzeAccount,
  getPanelData,
  getMonetizzeAccountList,
}

export default connect(mapStateToProps, mapDispatchToProps)(PanelMonetizze)