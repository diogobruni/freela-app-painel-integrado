import { objSerialize } from '../lib/helper.js'

const baseUri = 'https://dev.painelintegrado.com.br'
export default {
	endpoint: {
		loginSocial: (connection, token) => `${baseUri}/login-social/${connection}/${token}`,
		logout: `${baseUri}/logout`,
		login: `${baseUri}/app-login`,
		//getWidgets: `${baseUri}/xhr/get-widgets`,
		getPanelList: `${baseUri}/xhr/get-panels`,
		getPanel: (params) => `${baseUri}/xhr/get-widget-data?${objSerialize(params)}`,
		//getPanelFacebookAds: `${baseUri}/xhr/get-panel/facebook_ads`,
		getPanelFacebookAds: (params) => `${baseUri}/xhr/get-facebook-ads-data?${objSerialize(params)}`,
		//getPanelGoogleAdsense: `${baseUri}/xhr/get-panel/google_adsense`,
		getPanelGoogleAdsense: (params) => `${baseUri}/xhr/get-google-adsense-data?${objSerialize(params)}`,
		//getPanelMonetizze: `${baseUri}/xhr/get-panel/monetizze`,
		getPanelMonetizze: (params) => `${baseUri}/xhr/get-monetizze-data?${objSerialize(params)}`,
		getPanelCustom: (panelId) => `${baseUri}/xhr/get-panel/${panelId}`,
		getUserProfile: `${baseUri}/xhr/get-user`,
	}
}