export function getObjectValueOrDefault(data, field, defaultStr = '-') {
	if (data[field] != undefined && data[field].formattedValue) {
		return getVarOrDefault(data[field].formattedValue, defaultStr)
	} else {
		return defaultStr
	}
}

export function getVarOrDefault(value, defaultStr = '-') {
	return value != undefined ? value : defaultStr
}